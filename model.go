package wordpressconnector

import "time"

type PostResponse struct {
	ID        int       `json:"id"`
	Title     string    `json:"title"`
	FullTitle string    `json:"fullTitle"`
	Lead      string    `json:"lead"`
	PubDate   time.Time `json:"pubDate"`
	Links     struct {
		WebURI string `json:"webUri"`
		URI    string `json:"uri"`
	} `json:"links"`
	Type     string `json:"type"`
	Image    string `json:"image"`
	Metadata struct {
		Topics []struct {
			ID    int    `json:"id"`
			Name  string `json:"name"`
			Style struct {
				BackgroundColor string `json:"backgroundColor"`
				ForegroundColor string `json:"foregroundColor"`
			} `json:"style,omitempty"`
			AssociatedURL string `json:"associatedUrl"`
			MainTopic     bool   `json:"mainTopic"`
		} `json:"topics"`
		Authors []struct {
			ID          int      `json:"id"`
			Name        string   `json:"name"`
			URI         string   `json:"uri"`
			Image       string   `json:"image"`
			Description string   `json:"description"`
			Role        string   `json:"role"`
			CreditType  []string `json:"creditType"`
			Location    string   `json:"location"`
			FreeText    string   `json:"freeText"`
		} `json:"authors"`
		MainVideo bool `json:"main_video"`
		Gallery   struct {
			PhotosCount int `json:"photosCount"`
		} `json:"gallery"`
		Program struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
			Slug  string `json:"slug"`
		} `json:"program"`
		Duration string `json:"duration"`
		Headline bool   `json:"headline"`
		Featured bool   `json:"featured"`
	} `json:"metadata"`
	State         string `json:"state"`
	SquareImage   string `json:"square_image"`
	GalleryImages []struct {
		Caption string `json:"caption"`
		Credit  string `json:"credit"`
		URI     string `json:"uri"`
		Height  int    `json:"height"`
		Width   int    `json:"width"`
	} `json:"galleryImages"`
	Body            string `json:"body"`
	Premium         bool   `json:"premium"`
	CommentsEnabled bool   `json:"comments_enabled"`
}

type PostTopic struct {
	ID   int64
	Name string
}

type PostAuthor struct {
	ID    int64
	Name  string
	Image string
}

type PostProgram struct {
	ID    int64
	Title string
}
type PostStruct struct {
	Id          int64        `json:"id"`
	PubDate     time.Time    `json:"pubDate"`
	Title       string       `json:"title"`
	Image       string       `json:"image"`
	WebUri      string       `json:"webUri"`
	Type        string       `json:"type"`
	Fotogallery int          `json:"fotoGallery"`
	Topics      []PostTopic  `json:"topics"`
	Authors     []PostAuthor `json:"authors"`
	Program     PostProgram  `json:"program"`
}
