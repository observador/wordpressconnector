package wordpressconnector

import "testing"

func TestGetPostInfo(t *testing.T) {
	con := &Connector{
		ApiUrl: "https://api.observador.pt/wp/",
	}

	t.Log("get post for first time")
	p, cached, err := con.GetPostInfo(3994666)
	if err != nil || p.Id == 0 {
		t.Errorf("Error: %v", err)
	}

	t.Log("get post for second time")
	p, cached, err = con.GetPostInfo(3994666)
	if err != nil || p.Id == 0 {
		t.Errorf("Error: %v", err)
	}
	if !cached {
		t.Errorf("Error: %s", "the second time should be cached")
	}


}
