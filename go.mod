module bitbucket.org/observador/wordpressconnector

go 1.13

require (
	github.com/ReneKroon/ttlcache v1.6.0
	github.com/cenkalti/backoff/v4 v4.0.2
)
