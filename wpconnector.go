package wordpressconnector

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/ReneKroon/ttlcache"
	"github.com/cenkalti/backoff/v4"
)

type Connector struct {
	ApiUrl string
	cache  *ttlcache.Cache
}

// GetPostInfo call api observador v3 to get post info
func (c *Connector) GetPostInfo(postID int64) (p PostStruct, cached bool, err error) {
	if c.cache == nil {
		c.initCache()
	}

	strPostID := strconv.Itoa(int(postID))

	cachedValue, exists := c.cache.Get(strPostID)
	if exists {
		p = cachedValue.(PostStruct)
		return p, true, err
	}

	/**
	 * retry and backoff to get post data..
	 * como o wordpress sucks, quando envia a notificação os dados ainda nao estão guardados,
	 * então usamos um algoritmos de retry and backoff para repetir o pedido até termos os dados..
	 */

	firstRun := true
	nt := 0
	//retry 5 seconds after
	b := backoff.NewExponentialBackOff()
	b.InitialInterval = 5 * time.Second
	ticker := backoff.NewTicker(b)

	for range ticker.C {
		if firstRun {
			// in first run dont do anything
			firstRun = false
			continue
		}
		//get post info
		p, err = c.restGetPostInfo(postID)
		nt++
		if err != nil && err.Error() == "nodata" {
			ticker.Stop()
			break
		}
		// se o post nao for valido volta a tentar!
		if p.Type == "" || (p.Type != "opinion" && p.Image == "") || p.Title == "" || len(p.Authors) == 0 {
			if nt < 3 {
				continue
			}
			break
		}

		ticker.Stop()
		break
	}
	//se passar mais que 15min, return error
	if p.Type == "" || (p.Type != "opinion" && p.Image == "") || p.Title == "" || len(p.Authors) == 0 {
		return p, false, errors.New("i can't get contents")
	}

	//set cache for other worker!
	c.cache.Set(strPostID, p)

	return p, false, err
}

func (c *Connector) restGetPostInfo(postID int64) (p PostStruct, err error) {
	url := fmt.Sprintf("%sitems/id/%d", c.ApiUrl, postID)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("User-Agent", fmt.Sprintf("OBS%s", getUserAgent()))

	httpClient := &http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return p, err
	}

	defer resp.Body.Close()
	if resp.StatusCode <= 400 {
		defer resp.Body.Close()
		postBody, err := ioutil.ReadAll(resp.Body)

		if err == nil {
			var postResponse PostResponse

			err := json.Unmarshal(postBody, &postResponse)
			if err != nil {
				log.Println(err)
				return p, err
			}
			var post PostStruct
			post.Id = postID
			post.PubDate = postResponse.PubDate
			post.Title = postResponse.Title
			post.WebUri = postResponse.Links.WebURI
			post.Type = postResponse.Type
			post.Image = postResponse.Image
			if postResponse.GalleryImages != nil {
				post.Fotogallery = 1
			} else {
				post.Fotogallery = 0
			}

			if postResponse.Metadata.Topics != nil {
				for _, top := range postResponse.Metadata.Topics {
					var tmpTopic PostTopic
					tmpTopic.ID = int64(top.ID)
					tmpTopic.Name = top.Name
					post.Topics = append(post.Topics, tmpTopic)
				}

			}

			if postResponse.Metadata.Authors != nil {
				for _, aut := range postResponse.Metadata.Authors {
					if aut.CreditType[0] != "free_text" {
						var tmpAuthor PostAuthor
						tmpAuthor.ID = int64(aut.ID)
						tmpAuthor.Name = aut.Name

						tmpAuthor.Image = aut.Image

						post.Authors = append(post.Authors, tmpAuthor)
					}
				}

			}
			post.Program.ID = int64(postResponse.Metadata.Program.ID)
			post.Program.Title = postResponse.Metadata.Program.Title

			return post, nil
		}

	}

	return p, fmt.Errorf(resp.Status)
}

func (c *Connector) initCache() {
	c.cache = ttlcache.NewCache()
	c.cache.SetTTL(time.Duration(30 * time.Second))
}

func getUserAgent() string {
	return fmt.Sprintf("OBS%s", strings.Replace(strings.ToUpper(os.Args[0]), ".", "", -1))
}
